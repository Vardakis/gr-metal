INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_METAL metal)

FIND_PATH(
    METAL_INCLUDE_DIRS
    NAMES metal/api.h
    HINTS $ENV{METAL_DIR}/include
        ${PC_METAL_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    METAL_LIBRARIES
    NAMES gnuradio-metal
    HINTS $ENV{METAL_DIR}/lib
        ${PC_METAL_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(METAL DEFAULT_MSG METAL_LIBRARIES METAL_INCLUDE_DIRS)
MARK_AS_ADVANCED(METAL_LIBRARIES METAL_INCLUDE_DIRS)

