/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_METAL_MUSIC_BF_H
#define INCLUDED_METAL_MUSIC_BF_H

#include <metal/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace metal {

    /*!
     * \brief <+description of block+>
     * \ingroup metal
     *
     */
    class METAL_API music_bf : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<music_bf> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of metal::music_bf.
       *
       * To avoid accidental use of raw pointers, metal::music_bf's
       * constructor is in a private implementation
       * class. metal::music_bf::make is the public interface for
       * creating new instances.
       */
      static sptr make(size_t num_streams, size_t vlen, size_t ps_len, uint8_t array_t, double spacing, double lambda);
    };

  } // namespace metal
} // namespace gr

#endif /* INCLUDED_METAL_MUSIC_BF_H */

