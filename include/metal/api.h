/*
 * Copyright 2011 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * GNU Radio is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * GNU Radio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Radio; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_METAL_API_H
#define INCLUDED_METAL_API_H

#include <gnuradio/attributes.h>

#ifdef gnuradio_metal_EXPORTS
#  define METAL_API __GR_ATTR_EXPORT
#else
#  define METAL_API __GR_ATTR_IMPORT
#endif

typedef enum{
  NO_SYNC=0,
  PREAMBLE=1,
  POST_PREAMBLE=2,
  SYNC=3,
  IN_SYNC=4,
}state;

typedef enum{
  SYNCING=0,
  FIRST_SYNC_NOW=1,
  FIRST_SYNC_FOUND=2,
  FIRST_BUFFER_FILL=3,
  SYNCED=4,
  NO_PHASE_DIFF=5,
  FINE_SYNC=6,
  WINDOW_FILL=7,
  FIRST_SYNCING=8
}overall_state;

#endif /* INCLUDED_METAL_API_H */
