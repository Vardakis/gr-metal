# Angle of Arrival Estimation

## Overview of system

The system performs Direction of Arrival estimation in the azimuth plane. For accurate estimation, synchronization of the receiving streams must be performed using an a priori known training sequence.

##  Function of System

At startup, the system drops 'sampling_rate' samples in order to deal with spikes presented by the initialization of USRP devices and which leads to false peak detection by our algorithm. At the next stage the algorithm calculates the threshold which will be used for peak detection later. These two phases form the calibration process of our system. The calibration needs to be finished before the training sequence is transmitted by the transmitting device. As soon as the calibration is over, the training sequence is transmitted. At the receiver, cross correlation with the known sequence is applied to the input streams. When the sequence is detected, a peak value is produced as output by the detector. When the sequence is detected at all streams, the sample count difference between the moment it was detected for each stream is determined, and with appropriate apply of the calculated delays to the streams, these become time synchronized. Then, the DoA estimation begins.
In order to eliminate the frequency offset between the internal clocks of the USRP devices, an external clock must be used. The configuration of the clock is explained in the next sections.

## Demo

### Connecting Antennas, Clock and USRPs

The first thing to be done, should be the various cable connections. First
connect the two SMA ends of the cables of the clock, to the 10MHz input of the USRPs. 
Connect also the USB of the clock to the PC, to power it up.
Then, connect the four RX antennas to the four channels of the USRPs, two at each
one. The TX antenna must be connected to the USRP with serial number "EER04Z6BT". 
The exact channels at which the antennas must be connected, are explained in the next 
section

### Antennas Setup

The antennas must be placed with a certain distance between them, and also with a certain sequence.

<img src="demo_camad2.png" alt="The Pulpit Rock" width="250" height="300">

As depicted in the figure above, the antennas, when in line, define the x axis of the cartesian
coordinates system. The transmit antenna lies on the positive y axis of the system, opposite
of the RX antennas. We define the leftmost antenna (the one at the far end of the negative x axis)
as RX1, the next RX2, the first antenna on the positive x axis as RX3, and finally the rightmost antenna
at the positive x axis as RX4. This notation will be very important when connecting the antennas to the 
USRPs.

<img src="demo_camad_full_schema2.png" width="750" height="550">

The full demo setup is depicted in the figure above. It is of major importance
to connect each antenna to its respective channel according to the diagram above.
That means : The RX1 element must be connected to the RF B: RX2 channel of the
USRP with serial number "serial=EFR04ZDBT", and so on. As mentioned before, 
the TX element must be connected to the RF A: TX/RX channel of the USR with serial
number "EER04Z6BT".


The inter-element spacing must be λ/2 where λ is the wavelength of the signal being
transmitted. If the frequency allowed for transmit is the 2.49GHz, then the
spacing must be about 6cm. Be careful so all RX elements are properly alligned.

The transmitting element must be as far as possible, but with enough portion of
its transmitting energy reaching the RX elements to be detected. Since it is a
directional log-periodic triangular antenna, it must be kept in a perpendicular
position with respect to the array that is, the horizontal plane defined by the
antenna must be perpendicular to the ground. This way we achieve vertical polarization
and better detection.

### Calibration

After the various connections have been made and the devices are ready, it is time
for the calibration phase. For this reason the TX antenna must be placed as mentioned
before, opposite the array, i.e. at 90 degrees with respect to the axis defined
by the array. Also it must be kept at a perpendicular to the ground position during
the whole duration of the demo. 

Then the script detector.py must be executed. Appropriate messages on the console
will let the handler know when to start transmitting the training sequence, and
when the calibration has been concluded. After execution of the script, a few seconds
must go by without any action. At the appropriate time, a message will appear on the console,
instructing the handler to start transmitting the training sequence. This is done
by selecting the appropriate check box in the UI. When the calibration has finished,
another message will indicate that the process is completed, and that from now on
the handler can freely move the TX antenna around. Notice that because we only have
a linear array, the signal source will succesfully be  detected when moving in the
upper right quadrant of the coordinate system defined my the array i.e. from the
quadrant of positive x and positive y. If the source is placed in the upper left 
quadrant (negative x, positive y) the estimation will be wrapped around.

### Graphical Interface

The graphical interface is in fact a graph, where in the x axis lies the angle in degrees
, from 0 to 90, and in the y axis the intensity of the estimation. If the signal
source is placed at 60 degrees, then the point number 60 on the x axis, will have the
largest intensity on the y axis, as depicted in the figure below
<img src="gui.png" alt="The Pulpit Rock" width="700" height="400">

Although the x axis is labeled "Time", it is actually "Azimuth Angle"

The handler will notice before calibration that the angle displayed by the UI
will constantly change. That is because the RX streams are not calibrated. After
calibration has occured, the UI should indicate a signal coming from 90 degrees 
as in the figure below
<img src="90_deg.png" alt="The Pulpit Rock" width="700" height="400">

That is indeed the case, since we have placed the TX antenna at 90 degrees with
respect to the array in order to calibrate it. If after calibration the angle
indicated is not the 90 degrees one, the process should start from the beginning.




