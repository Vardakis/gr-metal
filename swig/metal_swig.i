/* -*- c++ -*- */

#define METAL_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "metal_swig_doc.i"

%{
#include "metal/music_bf.h"
#include "metal/rms_normalization.h"
#include "metal/synchronizer.h"
#include "metal/synchronizer_demod.h"
#include "metal/alligner.h"
#include "metal/peak_detector.h"
#include "metal/mvdr_beamformer.h"
#include "metal/tcp_rotctl_msg_source.h"
%}
%include "metal/music_bf.h"
GR_SWIG_BLOCK_MAGIC2(metal, music_bf);
%include "metal/rms_normalization.h"
GR_SWIG_BLOCK_MAGIC2(metal, rms_normalization);
%include "metal/synchronizer.h"
GR_SWIG_BLOCK_MAGIC2(metal, synchronizer);
%include "metal/synchronizer_demod.h"
GR_SWIG_BLOCK_MAGIC2(metal, synchronizer_demod);
%include "metal/alligner.h"
GR_SWIG_BLOCK_MAGIC2(metal, alligner);
%include "metal/peak_detector.h"
GR_SWIG_BLOCK_MAGIC2(metal, peak_detector);
%include "metal/mvdr_beamformer.h"
GR_SWIG_BLOCK_MAGIC2(metal, mvdr_beamformer);
%include "metal/tcp_rotctl_msg_source.h"
GR_SWIG_BLOCK_MAGIC2(metal, tcp_rotctl_msg_source);
