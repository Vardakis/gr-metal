#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Sync Tx
# Generated: Mon Mar 27 11:34:37 2017
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import metal
import sip
import sys
import time


class sync_tx(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Sync Tx")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Sync Tx")
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "sync_tx")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.sps = sps = 4
        self.samp_rate = samp_rate = 2e6
        
        self.taps = taps = firdes.root_raised_cosine(1.0, samp_rate, samp_rate/sps, 0.35, 11*sps)
          
        self.sine = sine = 0
        self.preamble = preamble = 0
        self.pre = pre = 1
        self.gain = gain = 60
        self.freq = freq = 2.1e9 
        
        self.fir_taps = fir_taps = firdes.low_pass(1.0, 1.0, (1.0/(sps*2))*0.95, 0.05*(1.0/(sps*2)), firdes.WIN_HAMMING, 6.76)
          
        
        self.bpsk = bpsk = digital.constellation_bpsk().base()
        

        ##################################################
        # Blocks
        ##################################################
        self._gain_range = Range(0, 100, 1, 60, 200)
        self._gain_win = RangeWidget(self._gain_range, self.set_gain, 'gain', "counter_slider", float)
        self.top_layout.addWidget(self._gain_win)
        self.uhd_usrp_sink_1 = uhd.usrp_sink(
        	",".join(("", "serial=EFR04ZDBT")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_1.set_clock_source('external', 0)
        self.uhd_usrp_sink_1.set_time_source('external', 0)
        self.uhd_usrp_sink_1.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_1.set_center_freq(freq  , 0)
        self.uhd_usrp_sink_1.set_gain(gain, 0)
        self.uhd_usrp_sink_1.set_bandwidth(samp_rate, 0)
        self._sine_options = (0, 1, )
        self._sine_labels = ('off', 'on', )
        self._sine_group_box = Qt.QGroupBox('Sine')
        self._sine_box = Qt.QVBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._sine_button_group = variable_chooser_button_group()
        self._sine_group_box.setLayout(self._sine_box)
        for i, label in enumerate(self._sine_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._sine_box.addWidget(radio_button)
        	self._sine_button_group.addButton(radio_button, i)
        self._sine_callback = lambda i: Qt.QMetaObject.invokeMethod(self._sine_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._sine_options.index(i)))
        self._sine_callback(self.sine)
        self._sine_button_group.buttonClicked[int].connect(
        	lambda i: self.set_sine(self._sine_options[i]))
        self.top_layout.addWidget(self._sine_group_box)
        self.root_raised_cosine_filter_0 = filter.interp_fir_filter_ccf(1, firdes.root_raised_cosine(
        	1, samp_rate, samp_rate/sps, 0.35, 11*sps))
        self.qtgui_freq_sink_x_2 = qtgui.freq_sink_c(
        	1024, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	0, #fc
        	samp_rate, #bw
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_freq_sink_x_2.set_update_time(0.010)
        self.qtgui_freq_sink_x_2.set_y_axis(-140, 10)
        self.qtgui_freq_sink_x_2.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_2.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_2.enable_autoscale(False)
        self.qtgui_freq_sink_x_2.enable_grid(False)
        self.qtgui_freq_sink_x_2.set_fft_average(1.0)
        self.qtgui_freq_sink_x_2.enable_axis_labels(True)
        self.qtgui_freq_sink_x_2.enable_control_panel(False)
        
        if not True:
          self.qtgui_freq_sink_x_2.disable_legend()
        
        if "complex" == "float" or "complex" == "msg_float":
          self.qtgui_freq_sink_x_2.set_plot_pos_half(not True)
        
        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_2.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_2.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_2.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_2.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_2.set_line_alpha(i, alphas[i])
        
        self._qtgui_freq_sink_x_2_win = sip.wrapinstance(self.qtgui_freq_sink_x_2.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_freq_sink_x_2_win)
        _preamble_push_button = Qt.QPushButton('Preamble')
        self._preamble_choices = {'Pressed': 1, 'Released': 0}
        _preamble_push_button.pressed.connect(lambda: self.set_preamble(self._preamble_choices['Pressed']))
        _preamble_push_button.released.connect(lambda: self.set_preamble(self._preamble_choices['Released']))
        self.top_layout.addWidget(_preamble_push_button)
        self._pre_options = (0, 1, )
        self._pre_labels = ('off', 'on', )
        self._pre_group_box = Qt.QGroupBox('Preamble')
        self._pre_box = Qt.QVBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._pre_button_group = variable_chooser_button_group()
        self._pre_group_box.setLayout(self._pre_box)
        for i, label in enumerate(self._pre_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._pre_box.addWidget(radio_button)
        	self._pre_button_group.addButton(radio_button, i)
        self._pre_callback = lambda i: Qt.QMetaObject.invokeMethod(self._pre_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._pre_options.index(i)))
        self._pre_callback(self.pre)
        self._pre_button_group.buttonClicked[int].connect(
        	lambda i: self.set_pre(self._pre_options[i]))
        self.top_layout.addWidget(self._pre_group_box)
        self.metal_synchronizer_0 = metal.synchronizer()
        self.interp_fir_filter_xxx_0 = filter.interp_fir_filter_ccc(sps, (fir_taps))
        self.interp_fir_filter_xxx_0.declare_sample_delay(0)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.interp_fir_filter_xxx_0, 0), (self.root_raised_cosine_filter_0, 0))    
        self.connect((self.metal_synchronizer_0, 0), (self.interp_fir_filter_xxx_0, 0))    
        self.connect((self.root_raised_cosine_filter_0, 0), (self.qtgui_freq_sink_x_2, 0))    
        self.connect((self.root_raised_cosine_filter_0, 0), (self.uhd_usrp_sink_1, 0))    

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "sync_tx")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.root_raised_cosine_filter_0.set_taps(firdes.root_raised_cosine(1, self.samp_rate, self.samp_rate/self.sps, 0.35, 11*self.sps))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_sink_1.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink_1.set_bandwidth(self.samp_rate, 0)
        self.root_raised_cosine_filter_0.set_taps(firdes.root_raised_cosine(1, self.samp_rate, self.samp_rate/self.sps, 0.35, 11*self.sps))
        self.qtgui_freq_sink_x_2.set_frequency_range(0, self.samp_rate)

    def get_taps(self):
        return self.taps

    def set_taps(self, taps):
        self.taps = taps

    def get_sine(self):
        return self.sine

    def set_sine(self, sine):
        self.sine = sine
        self._sine_callback(self.sine)

    def get_preamble(self):
        return self.preamble

    def set_preamble(self, preamble):
        self.preamble = preamble

    def get_pre(self):
        return self.pre

    def set_pre(self, pre):
        self.pre = pre
        self._pre_callback(self.pre)

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_sink_1.set_gain(self.gain, 0)
        	

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.uhd_usrp_sink_1.set_center_freq(self.freq  , 0)

    def get_fir_taps(self):
        return self.fir_taps

    def set_fir_taps(self, fir_taps):
        self.fir_taps = fir_taps
        self.interp_fir_filter_xxx_0.set_taps((self.fir_taps))

    def get_bpsk(self):
        return self.bpsk

    def set_bpsk(self, bpsk):
        self.bpsk = bpsk


def main(top_block_cls=sync_tx, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
