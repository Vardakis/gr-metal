/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_METAL_MUSIC_BF_IMPL_H
#define INCLUDED_METAL_MUSIC_BF_IMPL_H

#include <metal/music_bf.h>
#include <vector>

namespace gr {
  namespace metal {

    class music_bf_impl : public music_bf
    {
     private:
      size_t d_num_streams;
      size_t d_vlen;
      size_t d_ps_len;
      std::vector<std::vector<arma::cx_fvec>> d_steer_vecs{91,std::vector<arma::cx_fvec>(91)};
      uint8_t d_array_t;
      std::complex<double> d_temp[1024];
      double d_spacing;
      double d_lambda;
      double **d_positions;
     public:
      music_bf_impl(size_t num_streams, size_t vlen, size_t ps_len, uint8_t array_t, double spacing, double lambda);
      ~music_bf_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace metal
} // namespace gr

#endif /* INCLUDED_METAL_MUSIC_BF_IMPL_H */

