/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <gnuradio/fxpt_nco.h>
#include <volk/volk.h>
#include "synchronizer_impl.h"

namespace gr {
  namespace metal {

    synchronizer::sptr
    synchronizer::make()
    {
      return gnuradio::get_initial_sptr
        (new synchronizer_impl());
    }

    /*
     * The private constructor
     */
    synchronizer_impl::synchronizer_impl()
      : gr::sync_block("synchronizer",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1,1,sizeof(gr_complex))),
              d_last_index(0),
	      d_stop(false)
    {
	    d_bpsk = {gr_complex(-1.0, 0), gr_complex(1.0, 0)};
	    d_preamble_buf = {0x7a};
	    d_encoded_buf =(gr_complex*) malloc(1*8*sizeof(gr_complex) + 1000*sizeof(gr_complex));
	    encode(d_preamble_buf, d_encoded_buf,2);
	    //d_sin_wave = (float *)volk_malloc(50 * sizeof(float), 32);
	    /* Now fill the buffer with the appropriate sine wave */
	    gr::fxpt_nco nco;
	    //nco.set_freq(100e3);
	    //nco.sin(d_sin_wave, 50, 1.0);
	    //nco.sincos(d_encoded_buf + 80, 1000000, 1.0);
	    memset(d_encoded_buf + 8,0,1000*sizeof(gr_complex));

    }

    /*
     * Our virtual destructor.
     */
    synchronizer_impl::~synchronizer_impl()
    {
    }
    void
    synchronizer_impl::encode(std::vector<uint8_t> init_buf, gr_complex* encoded_buf, size_t length){
	    for(int i=0;i<length;i++){
		    uint8_t b = init_buf[i];
		    for(int j=0;j<8;j++){
			    encoded_buf[i*8 + j] =  d_bpsk[(b>>(7-j))&1];
		    }
	    }
    }
    int
    synchronizer_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      size_t count;
       gr_complex *out = (gr_complex *) output_items[0];
//      if (d_stop) {
//	memset(out,0,noutput_items*sizeof(gr_complex));
//	return noutput_items;
//      }
       for(int i=0;i<noutput_items;i++){

	      out[i]= d_encoded_buf[d_last_index];
	      d_last_index++;
	      d_last_index = d_last_index % (1*8 + 1000);
	      count = i+1;
	      if(d_last_index == 0){
//		//usleep(10000);
		//printf("\nend\n");
		//d_stop = true;
		//break;
	      }
       }
      // Tell runtime system how many output items we produced.

      return count;
    }

  } /* namespace metal */
} /* namespace gr */

