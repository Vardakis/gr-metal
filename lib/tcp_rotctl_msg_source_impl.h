/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_METAL_TCP_ROTCTL_MSG_SOURCE_IMPL_H
#define INCLUDED_METAL_TCP_ROTCTL_MSG_SOURCE_IMPL_H

#include <metal/tcp_rotctl_msg_source.h>

namespace gr {
  namespace metal {

    class tcp_rotctl_msg_source_impl : public tcp_rotctl_msg_source
    {
     private:
      pmt::pmt_t d_msg_out;
      boost::shared_ptr<boost::thread> d_thread;
      const std::string d_ip_addr;
      const size_t d_port;
      size_t d_interval;
      size_t d_mtu;
      bool d_running;

     public:
      tcp_rotctl_msg_source_impl(const std::string& ip, size_t port, size_t interval, size_t mtu);
      ~tcp_rotctl_msg_source_impl();

      void
      rotctl_client();

    };

  } // namespace metal
} // namespace gr

#endif /* INCLUDED_METAL_TCP_ROTCTL_MSG_SOURCE_IMPL_H */

