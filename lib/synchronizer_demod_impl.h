/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_METAL_SYNCHRONIZER_DEMOD_IMPL_H
#define INCLUDED_METAL_SYNCHRONIZER_DEMOD_IMPL_H

#include <metal/synchronizer_demod.h>
#include <volk/volk.h>

namespace gr {
  namespace metal {

    class synchronizer_demod_impl : public synchronizer_demod
    {
     private:
      size_t d_num_streams;
      size_t d_sample_counter;
      overall_state ov_state;
      size_t d_synced_streams;
      size_t d_init_filled_streams;
      double d_samp_rate;
      size_t d_ref_stream;
      size_t d_num_delayed_streams;
      gr_complex** d_delayed_samples_buf;
      std::complex<float> *d_phase_diffs;
      size_t *d_sample_difference;
      state *d_state_vector;
      uint8_t *d_constr_byte_vector;
      uint8_t *d_bit_counter;
      uint8_t *d_preamble_counter;
      double *d_fine_phase_diffs;
      std::complex<float>* d_fine_complex_shifts;
      size_t *d_cyc_buf_next_idx; /*Index of available next sample to be copied to the output stream*/
      size_t *d_filled_buf_rem; /*How many bytes remaining for the buffer of the stream
				   to fill initially*/

      pmt::pmt_t d_phase_shifts_out;
     public:
      synchronizer_demod_impl(size_t num_streams, double samp_rate);
      ~synchronizer_demod_impl();
      void setup_delays();
      void initial_buff_fill(gr_vector_const_void_star &in, gr_vector_void_star &out,int noutput_items);
      void delay_samples(gr_vector_const_void_star &in, gr_vector_void_star &out,int noutput_items);
      bool is_preamble(uint8_t byte, state st);
      bool is_sync1(uint8_t byte, state st, uint8_t preamble_counter, uint8_t bit_counter);
      bool is_sync2(uint8_t byte, state st);
      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace metal
} // namespace gr

#endif /* INCLUDED_METAL_SYNCHRONIZER_DEMOD_IMPL_H */

