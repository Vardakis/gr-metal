/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <armadillo>
#include <cmath>
#include <complex>
#include <math.h>
#include <fstream>
//#include <Eigen/Dense>
#include <gnuradio/io_signature.h>
#include "music_bf_impl.h"

namespace gr {
  namespace metal {
  //using namespace std;
  using namespace arma;

    music_bf::sptr
    music_bf::make(size_t num_streams, size_t vlen, size_t ps_len, uint8_t array_t, double spacing, double lambda)
    {
      return gnuradio::get_initial_sptr
        (new music_bf_impl(num_streams, vlen, ps_len, array_t, spacing, lambda));
    }

    /*
     * The private constructor
     */
    music_bf_impl::music_bf_impl (size_t num_streams, size_t vlen,
				  size_t ps_len, uint8_t array_t,
				  double spacing, double lambda) :
	    gr::sync_block (
		"music_bf",
		gr::io_signature::make (num_streams, num_streams,
					sizeof(std::complex<float>) * vlen),
		gr::io_signature::make (1, 1,
					sizeof(std::complex<float>) * ps_len)),
	    d_num_streams (num_streams),
	    d_vlen (vlen),
	    d_ps_len (ps_len),
	    d_array_t (array_t),
	    d_spacing (spacing),
	    d_lambda (lambda)
    {
      d_positions = (double**) malloc (3 * sizeof(double*));
      for (int i = 0; i < 3; i++) {
	d_positions[i] = (double*) malloc (d_num_streams * sizeof(uint8_t));
	memset (d_positions[i], 0, d_num_streams * sizeof(uint8_t));
      }
      double lim;
      int index;
      int dimension;
      switch (d_array_t)
	{
	case 0: /* Linear array */
	  lim = (d_num_streams - 1) / 2.0;
	  index = 0;
	  for (float i = -lim; i <= lim; i++) {
	    d_positions[0][index] = i * d_spacing;
	    printf ("%f\n", d_positions[0][index]);
	    index++;
	  }
	  break;
	case 1: /* Square array */
	  dimension = std::sqrt (d_num_streams);
	  if (!dimension * dimension == d_num_streams) {
	    printf (
		"Number of streams given not appropriate for square array\n");
	    break;
	  }
	  lim = (dimension - 1) / 2.0;
	  index=0;
	  for (float j = -lim; j <= lim; j++) {
	    for (float i = -lim; i <= lim; i++) {
	      d_positions[0][index] = i * d_spacing;
	      d_positions[1][index] = j * d_spacing;
	      printf ("%f\n", d_positions[0][index]);
	      index++;
	    }
	  }
	  break;
	case 2: /* Triangular array */
	  d_positions[0][0] = -d_spacing/2.0;
	  d_positions[1][0] = (-std::sqrt(3) * d_spacing)/6.0;
	  d_positions[0][1] = 0;
	  d_positions[1][1] = (std::sqrt(3) * d_spacing)/3.0;
	  d_positions[0][2] = d_spacing/2.0;
	  d_positions[1][2] = (-std::sqrt(3) * d_spacing)/6.0;
	  break;
	}
      float d;
      for (int i = 0; i < 90; i++) {
	for (int j = 0; j < 90; j++) {
	  cx_fvec steer_vec = cx_fvec (d_num_streams);
	  for (int el = 0; el < d_num_streams; el++) {
	    d = 2 * M_PI
		* (d_positions[1][el] * std::cos (j * M_PI / 180.0)
		    * std::sin (i * M_PI / 180.0)
		    + d_positions[0][el] * std::cos (j * M_PI / 180.0)
			* std::cos (i * M_PI / 180.0)
		    + d_positions[2][el] * std::sin (j * M_PI / 180.0))
		/ (d_lambda * 1.0);
	    std::complex<float> s (std::cos (d), std::sin (d));
	    steer_vec (el) = s;

	  }
	  d_steer_vecs[i][j] = steer_vec;
	}
      }

    }

    /*
     * Our virtual destructor.
     */
    music_bf_impl::~music_bf_impl()
    {
    }

    int
    music_bf_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
    	std::complex<float> **streams = (std::complex<float> **) &input_items[0];
    	std::complex<float> *out = (std::complex<float> *) output_items[0];
	for (int it = 0; it < noutput_items; it++) {
		cx_fmat fin;
		cx_fmat rxsig;
		for (int i = d_num_streams - 1; i >= 0; i--) {
			for(int j =0;j<d_vlen;j++){
				d_temp[j] = streams[i][it*d_vlen+j];
			}
			cx_fmat fin(&streams[i][it*d_vlen], 1, d_vlen, false, true);
			rxsig = join_cols(fin, rxsig);
		}
		cx_fmat R = (rxsig * rxsig.t()) / d_vlen;
		int s = 1;
		cx_fvec eigval;
		cx_fmat eigvec;
		eig_gen(eigval, eigvec, R);
		uvec indexes;
		uvec cols = linspace<uvec>(0, eigvec.n_cols - 1, eigvec.n_cols);
		indexes = sort_index(eigval, "descend");
		cx_fmat noise_sub = eigvec.submat(cols,
				indexes.subvec(s, d_num_streams - 1));
		cx_fmat res = cx_fmat(90, 1);
		for (int i = 0; i < 90; i++) {
			cx_fvec a = d_steer_vecs[i][0];
			cx_fmat res1 = 1 / (((a.t() * noise_sub) * noise_sub.t()) * a);
			res(i, 0) = res1(0);
		}
		memset(&out[it*d_ps_len], 0 , d_ps_len*sizeof(std::complex<float>));
		out[it*d_ps_len + res.index_max()] = res.max();
		//memcpy(&out[it*d_ps_len],res.memptr(),d_ps_len*sizeof(std::complex<float>));
	}


      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace metal */
} /* namespace gr */

