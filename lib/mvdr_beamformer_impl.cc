/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "mvdr_beamformer_impl.h"

#include <cmath>
#include <complex>
#include <math.h>
#include <fstream>
//#include <Eigen/Dense>
#include <gnuradio/io_signature.h>

namespace gr
{
  namespace metal
  {
    using namespace arma;

    mvdr_beamformer::sptr
    mvdr_beamformer::make (size_t num_streams, uint8_t array_t, double spacing,
			   double lambda)
    {
      return gnuradio::get_initial_sptr (
	  new mvdr_beamformer_impl (num_streams, array_t, spacing, lambda));
    }

    /*
     * The private constructor
     */
    mvdr_beamformer_impl::mvdr_beamformer_impl (size_t num_streams,
						uint8_t array_t, double spacing,
						double lambda) :
	    gr::sync_block (
		"mvdr_beamformer",
		gr::io_signature::make (num_streams, num_streams,
					sizeof(std::complex<float>)),
		gr::io_signature::make (1, 1, sizeof(std::complex<float>))),
	    d_num_streams (num_streams),
	    d_array_t (array_t),
	    d_spacing (spacing),
	    d_lambda (lambda),
	    d_azimuth(0),
	    d_elevation(0),
	    d_coord_in(pmt::mp("coordinates_in"))
    {
      set_max_noutput_items (1024);
      d_positions = (double**) malloc (3 * sizeof(double*));
      for (int i = 0; i < 3; i++) {
	d_positions[i] = (double*) malloc (d_num_streams * sizeof(uint8_t));
	memset (d_positions[i], 0, d_num_streams * sizeof(uint8_t));
      }
      double lim;
      int index;
      switch (d_array_t)
	{
	case 0: /* Linear array */
	  lim = (d_num_streams - 1) / 2.0;
	  index = 0;
	  for (float i = -lim; i <= lim; i++) {
	    d_positions[0][index] = i * d_spacing;
	    printf ("%f\n", d_positions[0][index]);
	    index++;
	  }
	  break;
	case 1: /* Square array */
	  int dimension = std::sqrt (d_num_streams);
	  if (!dimension * dimension == d_num_streams) {
	    printf (
		"Number of streams given not appropriate for square array\n");
	    break;
	  }
	  lim = (dimension - 1) / 2.0;
	  index = 0;
	  for (float j = -lim; j <= lim; j++) {
	    for (float i = -lim; i <= lim; i++) {
	      d_positions[0][index] = i * d_spacing;
	      d_positions[1][index] = j * d_spacing;
	      printf ("%f\n", d_positions[0][index]);
	      index++;
	    }
	  }
	  break;
	}
      float d;
      for (int i = 0; i < 360; i++) {
	for (int j = 0; j < 90; j++) {
	  cx_fvec steer_vec = cx_fvec (d_num_streams);
	  for (int el = 0; el < d_num_streams; el++) {
	    d = 2 * M_PI
		* (d_positions[1][el] * std::cos (j * M_PI / 180.0)
		    * std::sin (i * M_PI / 180.0)
		    + d_positions[0][el] * std::cos (j * M_PI / 180.0)
			* std::cos (i * M_PI / 180.0)
		    + d_positions[2][el] * std::sin (j * M_PI / 180.0))
		/ (d_lambda * 1.0);
	    std::complex<float> s (std::cos (d), std::sin (d));
	    steer_vec (el) = s;

	  }
	  d_steer_vecs[i][j] = steer_vec;
	}
      }
      /* Message port and their callback functions declaration*/
      message_port_register_in (d_coord_in);

      set_msg_handler (
	  d_coord_in,
	  boost::bind (&mvdr_beamformer_impl::update_coordinates, this, _1));
    }
    /*
     * Our virtual destructor.
     */
    mvdr_beamformer_impl::~mvdr_beamformer_impl ()
    {
    }

    int
    mvdr_beamformer_impl::work (int noutput_items,
				gr_vector_const_void_star &input_items,
				gr_vector_void_star &output_items)
    {
      std::complex<float> **streams = (std::complex<float> **) &input_items[0];
      std::complex<float> *out = (std::complex<float> *) output_items[0];
      cx_fmat fin;
      cx_fmat rxsig;
      for (int i = d_num_streams - 1; i >= 0; i--) {
	cx_fmat fin (&streams[i][0], 1, noutput_items, false, true);
	rxsig = join_cols (fin, rxsig);
      }
      cx_fmat R = (rxsig * rxsig.t ()) / noutput_items;
      cx_fmat L;
      try {
	L = 1 / (d_steer_vecs[d_azimuth][d_elevation].t () * solve (R.i (), d_steer_vecs[d_azimuth][d_elevation]));
      }
      catch (const std::exception& e) { // reference to the base of a polymorphic object
	//std::cout << e.what (); // information from length_error printed
	return noutput_items;
      }
      //L.print();
      cx_fmat w = L * d_steer_vecs[d_azimuth][d_elevation].st () * R.i ();
      for (int i = 0; i < noutput_items; i++) {
	cx_fmat o = w * rxsig.col (i);
	out[i] = o(0);
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

    void
    mvdr_beamformer_impl::update_coordinates(pmt::pmt_t msg){
      d_azimuth = pmt::to_uint64 (pmt::tuple_ref (msg, 0));
      d_elevation = pmt::to_uint64 (pmt::tuple_ref (msg, 1));
      printf("Azimuth = %d elevation = %d\n",d_azimuth, d_elevation);
    }

  } /* namespace metal */
} /* namespace gr */

