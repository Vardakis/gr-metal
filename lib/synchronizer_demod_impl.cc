/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "synchronizer_demod_impl.h"

#define MODPI(a) (((a)<-M_PI) ? a+2*M_PI : ((a)>M_PI ? a-2*M_PI : a))

namespace gr {
  namespace metal {

    synchronizer_demod::sptr
    synchronizer_demod::make(size_t num_streams, double samp_rate)
    {
      return gnuradio::get_initial_sptr
        (new synchronizer_demod_impl(num_streams, samp_rate));
    }

    /*
     * The private constructor
     */
    synchronizer_demod_impl::synchronizer_demod_impl(size_t num_streams, double samp_rate)
      : gr::sync_block("synchronizer_demod",
              gr::io_signature::make(num_streams, num_streams, sizeof(gr_complex)),
              gr::io_signature::make(0,0,0)),
	      d_num_streams(num_streams),
	      d_sample_counter(0),
	      ov_state(WINDOW_FILL),
	      d_synced_streams(0),
	      d_samp_rate(samp_rate),
	      d_ref_stream(0),
	      d_init_filled_streams(0),
	      d_num_delayed_streams(0),
	      d_phase_shifts_out(pmt::mp("shifts_out"))
    {
      d_delayed_samples_buf = NULL;
      d_state_vector = new state[num_streams];
      d_sample_difference = new  size_t[num_streams];
      d_constr_byte_vector= new uint8_t[num_streams];
      d_phase_diffs = new std::complex<float>[num_streams];
      d_cyc_buf_next_idx = new size_t[num_streams];
      d_filled_buf_rem= new size_t[num_streams];
      d_bit_counter = new uint8_t[num_streams];
      d_fine_phase_diffs = new double[num_streams];
      d_preamble_counter = new uint8_t[num_streams];
      d_fine_complex_shifts = new std::complex<float>[num_streams];
      for(int i=0; i< num_streams;i++){
	d_state_vector[i] = NO_SYNC;
	d_sample_difference[i] = 0;
	d_constr_byte_vector[i] = 0;
	d_cyc_buf_next_idx[i] = 0;
	d_filled_buf_rem[i] = 0;
	d_bit_counter[i] =0;
	d_preamble_counter[i] =0;
	d_fine_phase_diffs[i] =0;
      }
      message_port_register_out(d_phase_shifts_out);
    }

    /*
     * Our virtual destructor.
     */
    synchronizer_demod_impl::~synchronizer_demod_impl()
    {
    }

    int
    synchronizer_demod_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in;
 //     gr_complex *out;

     // out = (gr_complex *) output_items[0];
      uint8_t val;
      int b;
//      if(ov_state == NO_PHASE_DIFF){
//	for(int i =0; i<d_num_streams; i++){
//	  in = (const gr_complex *) input_items[i];
//	  out = (gr_complex *) output_items[i];
//	  memcpy(out,in, noutput_items*sizeof(gr_complex));
//	}
//	return noutput_items;
//      }
//      if(ov_state == FIRST_BUFFER_FILL){
//	 //std::cout<<"Preparing for first buffer fill..."<<std::endl;
//	initial_buff_fill(input_items,output_items,noutput_items);
//	if (ov_state == FIRST_BUFFER_FILL){
//	  return noutput_items;
//	}
//      }
      if((ov_state == SYNCED || (ov_state == FINE_SYNC) || (ov_state == NO_PHASE_DIFF))){
	//delay_samples(input_items,output_items,noutput_items);
	return noutput_items;
      }
      for (int items = 0; items < noutput_items; items++) {
	  if(d_sample_counter > 10000){
	    ov_state = SYNCING;
	    d_sample_counter = 0;
	    d_synced_streams = 0;
	     for(int i=0; i< d_num_streams;i++){
		d_state_vector[i] = NO_SYNC;
		d_sample_difference[i] = 0;
		d_constr_byte_vector[i] = 0;
		d_cyc_buf_next_idx[i] = 0;
		d_filled_buf_rem[i] = 0;
		d_bit_counter[i] =0;
		d_preamble_counter[i] =0;
		d_fine_phase_diffs[i] =0;
	      }
	     break;
	  }

	if(d_synced_streams == d_num_streams){
	  std::cout<<"All streams SYNCED!!!!"<<"Counter "<<d_sample_counter<<std::endl;
	  pmt::pmt_t tup = pmt::make_tuple(pmt::from_uint64(d_ref_stream),
					   pmt::make_blob(d_sample_difference, d_num_streams*sizeof(size_t)));
	  message_port_pub(d_phase_shifts_out, tup);
	  ov_state = SYNCED;
	  if(d_sample_counter == 0){
	    ov_state = NO_PHASE_DIFF;
	    std::cout<<"And nooo phase diff!!!!"<<std::endl;
	    break;
	  }
	  //ov_state = FINE_SYNC;

	  d_sample_counter =0;
	  //setup_delays();
	  d_synced_streams = 0;
	  break;
	}
	if(ov_state == FIRST_SYNC_FOUND)
	  d_sample_counter++;
	for (int i = 0; i < d_num_streams; i++) {

	  if (d_state_vector[i] != IN_SYNC) {
	    in = (const gr_complex *) input_items[i];
	    b = std::real (in[items]) > 0 ? 1 : 0;

	    d_constr_byte_vector[i] = (d_constr_byte_vector[i] << 1) | b;

	    //printf ("Stream %d bit %x\n", i, d_constr_byte_vector[i]);
	    if (is_preamble (d_constr_byte_vector[i],d_state_vector[i])) {
//	      printf ("PREAMBLE found by stream %d %x\n", i,
//		      d_constr_byte_vector[i]);
	      if(d_state_vector[i] == PREAMBLE)
		d_preamble_counter[i]++;
	      else{
		d_preamble_counter[i] =0;
		d_preamble_counter[i]++;
		d_state_vector[i] = PREAMBLE;
	      }
	      //d_constr_byte_vector[i] = 0;
	      d_bit_counter[i] = 1;
	    }
	    else if (is_sync1 (d_constr_byte_vector[i], d_state_vector[i], d_preamble_counter[i], d_bit_counter[i])) {
	      d_state_vector[i] = SYNC;
	      printf ("SYNC1 found by stream %d %x\n", i,
		      d_constr_byte_vector[i]);
	      d_bit_counter[i] = 1;
	    }
	    else if (is_sync2 (d_constr_byte_vector[i], d_state_vector[i])) {
	      printf ("SYNC2 found by stream %d %x\n", i,
		      d_constr_byte_vector[i]);
	      d_constr_byte_vector[i] = 0;
	      d_state_vector[i] = IN_SYNC;
	      d_synced_streams++;
	      if (d_synced_streams == d_num_streams)
		d_ref_stream = i;
	      d_sample_difference[i] = d_sample_counter;
	      printf("Stream %d storing sample diff %d\n",i,d_sample_difference[i]);
	      if (ov_state == SYNCING)
		ov_state = FIRST_SYNC_FOUND;
	    }
	    else {
	      if (d_state_vector[i] == NO_SYNC)
		continue;
	      if (d_bit_counter[i] > 8) {
		//printf("Counter expired stream %d\n",i);
		d_bit_counter[i] = 0;
		d_state_vector[i] = NO_SYNC;
	      }
	      if (d_state_vector[i] == PREAMBLE || d_state_vector[i] == SYNC){
		d_bit_counter[i]++;
		//printf("Increasing counter\n");
	      }
	      if (d_state_vector[i] == PREAMBLE)
		d_state_vector[i] == POST_PREAMBLE;
	      if(d_state_vector[i] == POST_PREAMBLE && d_bit_counter[i] <8)
		d_bit_counter[i]++;
	    }
	  }
	  else
	    continue;
	  }
      }


      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

    bool
    synchronizer_demod_impl::is_preamble(uint8_t byte, state st){
      if(st == SYNC)
	return false;
      uint8_t val1 = byte ^ 0x33;
      uint8_t val2 = byte ^ 0xcc;
      uint8_t val3 = byte ^ 0x66;
      uint8_t val4 = byte ^ 0x99;
      if(val1 == 0)// || std::log2 (val1) == std::floor (std::log2 (val1)))
	return true;
      if(val2 == 0)// || std::log2 (val2) == std::floor (std::log2 (val2)))
      	return true;
      if(val3 == 0)// || std::log2 (val3) == std::floor (std::log2 (val3)))
      	return true;
      if(val4 == 0)// || std::log2 (val4) == std::floor (std::log2 (val4)))
      	return true;
      return false;
    }

    bool
    synchronizer_demod_impl::is_sync1(uint8_t byte, state st, uint8_t preamble_counter, uint8_t bit_counter){
      uint8_t val = byte ^ 0x7a;
      if((preamble_counter < 24) || (bit_counter == 0) || (bit_counter >8))
	return false;
      if ((val == 0) || std::log2 (val) == std::floor (std::log2 (val)))
	return true;
      return false;
    }
    bool
    synchronizer_demod_impl::is_sync2(uint8_t byte, state st){
      uint8_t val = byte ^ 0x0e;
      if ((st == SYNC) && (val == 0 || std::log2 (val) == std::floor (std::log2 (val))))
	return true;
      return false;
    }
    void
    synchronizer_demod_impl::setup_delays(){
      d_delayed_samples_buf = (gr_complex**)malloc(d_num_streams*sizeof(gr_complex*));
      size_t temp_diffs[d_num_streams];
      memcpy(temp_diffs,d_sample_difference, d_num_streams*sizeof(size_t));
      for(int i =0; i<d_num_streams; i++){
	//std::cout<<"Stream "<<i <<"Sample diff" << d_sample_difference[d_ref_stream] << "New Diff "<< d_sample_difference[d_ref_stream] - d_sample_difference[i]<<std::endl;
	d_sample_difference[i] = temp_diffs[d_ref_stream] - temp_diffs[i];
	d_filled_buf_rem[i] = d_sample_difference[i];
	std::cout<<"Stream "<<i <<"Sample diff" << d_sample_difference[i] << "Last stream "<<d_ref_stream <<std::endl;
	d_delayed_samples_buf[i] = (gr_complex*)malloc(d_sample_difference[i]*sizeof(gr_complex));
	if(d_sample_difference[i] > 0)
	  d_num_delayed_streams++;
      }
      ov_state = FIRST_BUFFER_FILL;
      std::cout<<"Delay buffers are set up!!!!"<<std::endl;
      return;

    }

    void
    synchronizer_demod_impl::initial_buff_fill (gr_vector_const_void_star &input_items,
						gr_vector_void_star &out,
						int noutput_items)
    {
      const gr_complex* in;
      for (int i = 0; i < d_num_streams; i++) {
	//std::cout<<"Stream "<<i << "Remaining "<<d_filled_buf_rem[i]<<std::endl;
	//sleep(1);
	in = (const gr_complex *) input_items[i];
	if (d_filled_buf_rem[i] > 0) {
	  //std::cout<<"Remaining "<<d_filled_buf_rem[i]<<std::endl;
	  //std::cout<<"Noutput "<<noutput_items<<std::endl;
	  if(noutput_items >= d_filled_buf_rem[i]){
	    std::cout<<"Cyc index "<<d_cyc_buf_next_idx[i]<<std::endl;
	    if(d_cyc_buf_next_idx[i] == 0){
	      memcpy(&d_delayed_samples_buf[i][d_cyc_buf_next_idx[i]],&in[noutput_items-d_filled_buf_rem[i]],d_filled_buf_rem[i]*sizeof(gr_complex));
	    }
	    else{
	      memcpy(&d_delayed_samples_buf[i][d_cyc_buf_next_idx[i]],in,d_filled_buf_rem[i]*sizeof(gr_complex));
	    }
	    d_init_filled_streams++;
	    d_cyc_buf_next_idx[i] = 0;
	    d_filled_buf_rem[i]=0;
	  }
	  else{
	    memcpy(&d_delayed_samples_buf[i][d_cyc_buf_next_idx[i]],&in[0],noutput_items*sizeof(gr_complex));
	    d_cyc_buf_next_idx[i]+=noutput_items;
	    d_filled_buf_rem[i]-=noutput_items;
	  }
	}
      }
      if(d_init_filled_streams == d_num_delayed_streams){
	ov_state = FINE_SYNC;
	//std::cout<<"All streams filled initial buffers!!!!"<<std::endl;
      }
      return;
    }
    void
    synchronizer_demod_impl::delay_samples (
	gr_vector_const_void_star &input_items,
	gr_vector_void_star &output_items, int noutput_items)
    {
      const gr_complex* in;
      gr_complex* out;
      size_t num_first_copy = 0;
      size_t num_sec_copy = 0;
      for (int i = 0; i < d_num_streams; i++) {
	in = (const gr_complex *) input_items[i];
	out = (gr_complex *) output_items[i];
	if (d_sample_difference[i] == 0) {
	  memcpy (out, in, noutput_items * sizeof(gr_complex));
	  if (i > 0) {
	   // std::cout << "Ov state " << ov_state << std::endl;
	    if (ov_state == FINE_SYNC) {
	      gr_complex* r = (gr_complex *) output_items[0];
	      gr_complex* n = (gr_complex *) output_items[i];
	      for(int j=0;j<noutput_items;j++){
	      std::complex<float> ref = r[j];
	      std::complex<float> now = n[j];
	      d_fine_phase_diffs[i] = (d_fine_phase_diffs[i]
		  + MODPI(std::atan2 (std::abs (ref), std::abs (now))));
	      //std::cout << "Phase diff1 " << MODPI(std::atan2 (std::abs (ref), std::abs (now))) << std::endl;
	      }
	    }
	    else if (ov_state == SYNCED) { /*Perform fine shift*/
	      //volk_32fc_s32fc_multiply_32fc (out, out, d_fine_complex_shifts[i],
		//			     noutput_items);
	    }
	  }
	  continue;
	}
	size_t len;
	if (noutput_items >= d_sample_difference[i]) {
	  len = d_sample_difference[i];
	}
	else {
	  len = noutput_items;
	}
	if (d_cyc_buf_next_idx[i] + len <= d_sample_difference[i]) {
	  memcpy (out, &d_delayed_samples_buf[i][d_cyc_buf_next_idx[i]],
		  len * sizeof(gr_complex));
	  memcpy (&d_delayed_samples_buf[i][d_cyc_buf_next_idx[i]], &in[noutput_items - len],
		  len * sizeof(gr_complex));
	  if (noutput_items > len){
	    memcpy (out + len, in, (noutput_items - len) * sizeof(gr_complex));

	  }
	  d_cyc_buf_next_idx[i] = (d_cyc_buf_next_idx[i] + len)
	      % d_sample_difference[i];
	}
	else {
	  num_first_copy = d_sample_difference[i]
	      - d_cyc_buf_next_idx[i];
	  num_sec_copy = len - num_first_copy;
	  memcpy (out, &d_delayed_samples_buf[i][d_cyc_buf_next_idx[i]],
		  num_first_copy * sizeof(gr_complex));
	  memcpy (out + num_first_copy, d_delayed_samples_buf[i],
		  num_sec_copy * sizeof(gr_complex));
	  memcpy (&d_delayed_samples_buf[i][d_cyc_buf_next_idx[i]], &in[noutput_items - len],
		  num_first_copy * sizeof(gr_complex));
	  memcpy (&d_delayed_samples_buf[i][0], &in[noutput_items - num_sec_copy],
		  num_sec_copy * sizeof(gr_complex));
	  d_cyc_buf_next_idx[i] = num_sec_copy % d_sample_difference[i];
	  if (noutput_items > len){
	    memcpy (out + len, in, (noutput_items - len) * sizeof(gr_complex));
	  }
	}
	//std::cout << "Ov state "<<ov_state<<std::endl;
	if (i > 0) {
	 // std::cout << "Ov state "<<ov_state<<std::endl;
	  if (ov_state == FINE_SYNC) {
	    gr_complex* r = (gr_complex *) output_items[0];
	    gr_complex* n = (gr_complex *) output_items[i];
	    for(int j=0;j<noutput_items;j++){
	    std::complex<float> ref = r[j];
	    std::complex<float> now = n[j];
	    d_fine_phase_diffs[i] = (d_fine_phase_diffs[i]
		+ MODPI(std::atan2 (std::abs (ref), std::abs (now))));
	    //std::cout << "Phase diff2 "<< MODPI(std::atan2 (std::abs (ref), std::abs (now)))<<std::endl;
	    }
	  }
	  else if(ov_state == SYNCED) { /*Perform fine shift*/
	    //volk_32fc_s32fc_multiply_32fc(out, out,d_fine_complex_shifts[i] , noutput_items);
	  }
	}

      }
      if((ov_state == FINE_SYNC))
	d_sample_counter+=noutput_items;
      if((d_sample_counter > 10000) && (ov_state == FINE_SYNC)){
	ov_state = SYNCED;
	for(int i = 1;i<d_num_streams;i++){
	  std::complex<float> c(std::cos(d_fine_phase_diffs[i]/d_sample_counter), -std::sin(d_fine_phase_diffs[i]/d_sample_counter));
	  d_fine_complex_shifts[i] = c;
	  std::cout << "Phase diff "<<c<<std::endl;
	}
      }

    }


  } /* namespace metal */
} /* namespace gr */

