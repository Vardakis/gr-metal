/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_METAL_SYNCHRONIZER_IMPL_H
#define INCLUDED_METAL_SYNCHRONIZER_IMPL_H

#include <metal/synchronizer.h>
#include <vector>

namespace gr {
  namespace metal {

    class synchronizer_impl : public synchronizer
    {
     private:
      std::vector<uint8_t> d_preamble_buf;
      gr_complex *d_encoded_buf;
      std::vector<gr_complex> d_bpsk;
      size_t d_last_index;
      bool d_stop;
      float* d_sin_wave;
      size_t d_counter;

     public:
      synchronizer_impl();
      ~synchronizer_impl();
      void encode(std::vector<uint8_t> init_buf, gr_complex* encoded_buf,size_t length);

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace metal
} // namespace gr

#endif /* INCLUDED_METAL_SYNCHRONIZER_IMPL_H */

