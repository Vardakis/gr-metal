/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include "tcp_rotctl_msg_source_impl.h"
#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <iostream>
#include <sstream>

namespace gr {
  namespace metal {

    tcp_rotctl_msg_source::sptr
    tcp_rotctl_msg_source::make(const std::string& ip, size_t port, size_t interval, size_t mtu)
    {
      return gnuradio::get_initial_sptr
        (new tcp_rotctl_msg_source_impl(ip, port, interval, mtu));
    }

    /*
     * The private constructor
     */
    tcp_rotctl_msg_source_impl::tcp_rotctl_msg_source_impl(const std::string& ip, size_t port, size_t interval, size_t mtu)
      : gr::block("tcp_rotctl_msg_source",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(0, 0, 0)),
	      d_msg_out(pmt::mp("coordinates")),
	      d_ip_addr(ip),
	      d_port(port),
	      d_interval(interval),
	      d_mtu(mtu),
	      d_running(true)
    {
      message_port_register_out(d_msg_out);
      d_thread = boost::shared_ptr<boost::thread> (
                new boost::thread (
                    boost::bind (&tcp_rotctl_msg_source_impl::rotctl_client, this)));
    }

    void
    tcp_rotctl_msg_source_impl::rotctl_client ()
    {
      int sock;
      int accept_sock;
      int ret;
      struct sockaddr_in sin;
      struct sockaddr client_addr;
      socklen_t client_addr_len;

      int optval = 1;
      char* buffer;
      sock = socket (AF_INET, SOCK_STREAM, 0);
      if (sock < 0) {
	perror ("ERROR opening socket");
	exit (0);
      }
      memset (&sin, 0, sizeof(struct sockaddr_in));
      memset (&client_addr, 0, sizeof(struct sockaddr));
      sin.sin_family = AF_INET;
      sin.sin_addr.s_addr = INADDR_ANY;
      sin.sin_port = htons (d_port);
      int ip = inet_aton (d_ip_addr.c_str (), &(sin.sin_addr));
      if (ip == 0) {
	perror ("server IP address incorrect");
	close (sock);
	exit (0);
      }

      if (bind (sock, (struct sockaddr *) &sin, sizeof(struct sockaddr_in))
	  == -1) {
	perror ("TCP bind");
	close (sock);
	exit (0);
      }

      if (listen (sock, 1000) == -1) {
	perror ("TCP listen");
	close (sock);
	exit (0);
      }
      buffer = new char[d_mtu];

      while (d_running) {
	int azimuth;
	int elevation;
	accept_sock = accept (sock, &client_addr, &client_addr_len);
	if (accept_sock <= 0) {
	  perror ("TCP accept");
	  exit (0);
	}
	if (setsockopt (accept_sock, IPPROTO_TCP, TCP_NODELAY, &optval,
			sizeof(int)) < 0) {
	  perror ("TCP setsockopt TCP_NODELAY");
	  shutdown (accept_sock, SHUT_RDWR);
	  close (accept_sock);
	  exit (0);
	}
	while ((ret = recv (accept_sock, buffer, d_mtu, 0)) > 0 && d_running) {
	  std::string str (buffer, d_mtu);
	  std::cout << str << std::endl;
	  static const char *cmd = "RPRT 0\n";
	  int p = send (accept_sock, cmd, strnlen (cmd, 7), 0);
	  size_t pos;
	  char delimiter = ' ';
	  std::vector<std::string> internal;
	  std::stringstream ss(str); // Turn the string into a stream.
	  std::string tok;
	  while(getline(ss, tok, delimiter)) {
	    internal.push_back(tok);
	  }
	  if ((int) std::round (std::stod (internal[2])) > 0) {
	    if ((int) std::round (std::stod (internal[1])) != azimuth
		|| ((int) std::round (std::stod (internal[2])) != elevation)) {
	      azimuth = (int) std::round (std::stod (internal[1]));
	      elevation = (int) std::round (std::stod (internal[2]));

	      pmt::pmt_t tup = make_tuple (pmt::from_uint64 (azimuth),
					   pmt::from_uint64 (elevation));
	      message_port_pub (d_msg_out, tup);
	    }
	  }
	}
      }
    }

    /*
     * Our virtual destructor.
     */
    tcp_rotctl_msg_source_impl::~tcp_rotctl_msg_source_impl()
    {
      d_running = false;
    }


  } /* namespace metal */
} /* namespace gr */

